import PropTypes from 'prop-types'
import React from 'react'

import './LetterToFind.css'

const LetterToFind = ({ letter, feedback }) => (
    <div className={`letterToFind ${feedback}`}>
        <span className="symbol">
            {feedback === 'hidden' ? '_' : letter}
        </span>
    </div>
)

LetterToFind.propTypes = {
  letter: PropTypes.string.isRequired,
  feedback: PropTypes.oneOf([
    'hidden',
    'visible'
  ]).isRequired,
  index: PropTypes.number.isRequired
}

export default LetterToFind
