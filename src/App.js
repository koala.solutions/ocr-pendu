import React, { Component } from 'react'
import './App.css'
import Letter from './Letter'
import LetterToFind from './LetterToFind'
import Try from './Try'

const LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
const ARRAY_WORDSTOFIND = ['JAVASCRIPT', 'TENTACULE', 'REACTNATIVE', 'WEBSTORM', 'CHROMIUM']
let ARRAY_LETTERSTOFIND = []

class App extends Component {
  state = {
    letters: this.generateLetters(),
    theWinWord: this.generateWordToFind(),
    selectedGoodLetters: [],
    badTryCounter: 0,
    lettersToFind: ARRAY_LETTERSTOFIND,
    triedLetters: []
  }

  handleLetterClick = (index, letter) => {
    const {lettersToFind, badTryCounter, selectedGoodLetters} = this.state
    this.setState({triedLetters: [...this.state.triedLetters, letter]})
    if(lettersToFind.includes(letter)){
      if(!selectedGoodLetters.includes(letter)){
        this.setState({selectedGoodLetters: [...this.state.selectedGoodLetters, letter]})
        return
      }
    }else{
      const newBadTryCounter = badTryCounter + 1
      this.setState({badTryCounter: newBadTryCounter})
      return
    }
  }

  handleGameOverClick = () => {
    this.setState({letters: this.generateLetters()})
    this.setState({theWinWord: this.generateWordToFind()})
    this.setState({selectedGoodLetters: []})
    this.setState({badTryCounter: 0})
    this.setState({lettersToFind: ARRAY_LETTERSTOFIND})
    this.setState({triedLetters: []})
  }

  generateLetters(){
    const result = LETTERS.split('')
    return result
  }

  generateWordToFind(){
    const term = ARRAY_WORDSTOFIND[Math.floor(Math.random()*ARRAY_WORDSTOFIND.length)]
    ARRAY_LETTERSTOFIND = term.split('')
    return term
  }

  getFeedbackLetterToFind(letter) {
    const {selectedGoodLetters} = this.state
    const letterMatched = selectedGoodLetters.includes(letter)
    return letterMatched ? 'visible' : 'hidden'
  }

  getFeedbackLetter(letter){
    const {triedLetters} = this.state
    const letterMatched = triedLetters.includes(letter)
    return letterMatched ? 'tried' : ''
  }

  getTriesNumber(){
    const {badTryCounter} = this.state
    return badTryCounter
  }

  hasWon(){
    const {lettersToFind, selectedGoodLetters} = this.state
    const uniqueLettersToFind = lettersToFind.filter(this.onlyUnique)
    if(uniqueLettersToFind.length === selectedGoodLetters.length)
      return true
    else
      return false
  }

  hasLost(){
    const {badTryCounter} = this.state
    if(badTryCounter === 10){
      return true
    }else{
      return false
    }
  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  render() {
    const {letters, lettersToFind} = this.state
    const wonGame = this.hasWon()
    let display
    if(!wonGame){
      if(this.hasLost()){
        display = <div className='try'><button onClick={this.handleGameOverClick}>Partie perdue</button></div>
      }else{
        display = letters.map((letter, index) => (
                      <Letter
                          letter={letter}
                          onClick={this.handleLetterClick}
                          index={index}
                          feedback={this.getFeedbackLetter(letter)}
                          key={index}
                      />
                  ))
      }
    }else{
      display = <div className='try'><button onClick={this.handleGameOverClick}>Partie terminée</button></div>
    }
    return (
        <div className="pendu">
          {lettersToFind.map((letter, index) => (
              <LetterToFind
                  letter={letter}
                  feedback={this.getFeedbackLetterToFind(letter)}
                  index={index}
                  key={index}
              />
          ))}
          <br/><br/>
          {display}
          <Try
              triesNumber={this.getTriesNumber()}
          />
        </div>
    )
  }
}

export default App;
