import PropTypes from 'prop-types'
import React from 'react'

import './Letter.css'

const Letter = ({ letter, index, onClick, feedback }) => (
    <div className={`letter ${feedback}`} onClick={() => onClick(index, letter)}>
        <span className="symbol">
            {letter}
        </span>
    </div>
)

Letter.propTypes = {
  letter: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default Letter
