import PropTypes from 'prop-types'
import React from 'react'

import './Try.css'

const Try = ({ triesNumber }) => (
    <div className={`try`}>
        <span className="tryNumber">
            Mauvais essais : {triesNumber}
        </span>
    </div>
)

Try.propTypes = {
  triesNumber: PropTypes.number.isRequired
}

export default Try
